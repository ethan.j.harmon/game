/*
* Copyright (c) 2022 Daniél Kerkmann <daniel@kerkmann.dev>
* and the contributors of the MarsValley project.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use bevy::prelude::*;

/// CameraMotion represents the rotation of a camera with pitch and yaw and provides settings for
/// motion sensitivity and enable/disable the camera motion
#[derive(Component, Debug)]
pub struct CameraMotion {
    pub pitch: f32,
    pub yaw: f32,
    pub sense: f32,
}

impl Default for CameraMotion {
    fn default() -> Self {
        Self {
            pitch: 0.0,
            yaw: 0.0,
            sense: 10.0,
        }
    }
}

impl CameraMotion {
    /// calculate the horizontal transformation from a motion delta.
    pub fn get_horizontal_rotation(&mut self, delta: &Vec2, elapsed: f32) -> Quat {
        self.yaw -= delta.x * self.sense * elapsed;
        Quat::from_axis_angle(Vec3::Y, self.yaw.to_radians())
    }

    /// calculate the vertical transformation from a motion delta.
    pub fn get_vertical_rotation(&mut self, delta: &Vec2, elapsed: f32) -> Quat {
        self.pitch += delta.y * self.sense * elapsed;
        self.pitch = self.pitch.clamp(-89.0, 89.9);
        Quat::from_axis_angle(-Vec3::X, self.pitch.to_radians())
    }
}

#[derive(Component, Debug)]
pub struct CameraMovement {
    pub speed: f32,
}

impl Default for CameraMovement {
    fn default() -> Self {
        Self { speed: 10. }
    }
}

#[derive(Debug, PartialEq)]
pub struct CameraDirection {
    pub x: Vec3,
    pub z: Vec3,
    pub y: Vec3,
}

#[cfg(test)]
mod test {
    use super::*;

    const DEFAULT_SENSE: f32 = 10.0;

    #[test]
    fn horizontal_camera_rotation_negative() {
        let delta = Vec2::new(-2.0, 0.0);
        let mut camera_motion = CameraMotion::default();
        let rotation = camera_motion.get_horizontal_rotation(&delta, 1.0);
        let expected_rotation = Quat::from_axis_angle(Vec3::Y, (2.0 * DEFAULT_SENSE).to_radians());

        assert_eq!(2.0 * DEFAULT_SENSE, camera_motion.yaw);
        assert_eq!(expected_rotation, rotation);
    }

    #[test]
    fn horizontal_camera_rotation_positiv() {
        let delta = Vec2::new(2.0, 0.0);
        let mut camera_motion = CameraMotion::default();
        let rotation = camera_motion.get_horizontal_rotation(&delta, 1.0);
        let expected_rotation = Quat::from_axis_angle(Vec3::Y, -(2.0 * DEFAULT_SENSE).to_radians());

        assert_eq!(-2.0 * DEFAULT_SENSE, camera_motion.yaw);
        assert_eq!(expected_rotation, rotation);
    }

    #[test]
    fn vertical_camera_rotation_negative() {
        let delta = Vec2::new(0.0, -2.0);
        let mut camera_motion = CameraMotion::default();
        let rotation = camera_motion.get_vertical_rotation(&delta, 1.0);
        let expected_rotation =
            Quat::from_axis_angle(-Vec3::X, -(2.0 * DEFAULT_SENSE).to_radians());

        assert_eq!(-2.0 * DEFAULT_SENSE, camera_motion.pitch);
        assert_eq!(expected_rotation, rotation);
    }

    #[test]
    fn vertical_camera_rotation_positiv() {
        let delta = Vec2::new(0.0, 2.0);
        let mut camera_motion = CameraMotion::default();
        let rotation = camera_motion.get_vertical_rotation(&delta, 1.0);
        let expected_rotation = Quat::from_axis_angle(-Vec3::X, (2.0 * DEFAULT_SENSE).to_radians());

        assert_eq!(2.0 * DEFAULT_SENSE, camera_motion.pitch);
        assert_eq!(expected_rotation, rotation);
    }
}
