/*
* Copyright (c) 2022 Daniél Kerkmann <daniel@kerkmann.dev>
* and the contributors of the MarsValley project.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use bevy::{
    input::mouse::{MouseMotion, MouseWheel},
    prelude::*,
};

use crate::components::{CameraMotion, CameraMovement, PlayerMovement};
use crate::states::MouseState;

pub struct CameraPlugin;

#[derive(Component)]
struct PlayerCameraMovement {
    pitch: f32,
    zoom: f32,
}

impl Plugin for CameraPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(PlayerCameraMovement {
            pitch: 0.0,
            zoom: 15.0,
        })
        .add_system_set(SystemSet::on_enter(MouseState::Uncatched).with_system(release_mouse))
        .add_system_set(SystemSet::on_exit(MouseState::Uncatched).with_system(catch_mouse))
        .add_system_set(
            SystemSet::on_update(MouseState::PlayerCamera)
                .with_system(player_camera_motion_system)
                .with_system(player_camera_movement_system),
        )
        .add_system_set(
            SystemSet::on_update(MouseState::FlyCamera)
                .with_system(fly_camera_mouse_movement_system)
                .with_system(fly_camera_keyboard_movement_system),
        )
        .add_system(toggle_mouse_state_system);
    }
}

fn catch_mouse(mut windows: ResMut<Windows>) {
    let window = windows.get_primary_mut().unwrap();

    window.set_cursor_visibility(false);
    window.set_cursor_lock_mode(true);
}

fn release_mouse(mut windows: ResMut<Windows>) {
    let window = windows.get_primary_mut().unwrap();

    window.set_cursor_visibility(true);
    window.set_cursor_lock_mode(false);
}

/// system that enables/disables the camera motion and movement by pressing `T`. If enabled the
/// window is locking and hiding the cursor.
fn toggle_mouse_state_system(
    input: Res<Input<KeyCode>>,
    mut mouse_state: ResMut<State<MouseState>>,
) {
    if input.just_pressed(KeyCode::T) {
        match mouse_state.current() {
            MouseState::Uncatched => mouse_state.set(MouseState::PlayerCamera).unwrap(),
            MouseState::PlayerCamera => mouse_state.set(MouseState::FlyCamera).unwrap(),
            MouseState::FlyCamera => mouse_state.set(MouseState::Uncatched).unwrap(),
        }
    }
}

/// system that implements camera motion by reading [MouseMotion](MouseMotion) events and rotate
/// the camera based on the read delta
fn fly_camera_mouse_movement_system(
    time: Res<Time>,
    mut mouse_motion_event_reader: EventReader<MouseMotion>,
    mut camera_query: Query<(&mut CameraMotion, &mut Transform)>,
) {
    let mut delta: Vec2 = Vec2::ZERO;
    for event in mouse_motion_event_reader.iter() {
        delta += event.delta;
    }

    let elapsed = time.delta_seconds();
    let (mut camera_motion, mut camera_transform) = camera_query.single_mut();

    let yaw_rot = camera_motion.get_horizontal_rotation(&delta, elapsed);
    let pitch_rot = camera_motion.get_vertical_rotation(&delta, elapsed);
    camera_transform.rotation = yaw_rot * pitch_rot;
}

// System that implements camera movement based on the camera's rotation
fn fly_camera_keyboard_movement_system(
    input: Res<Input<KeyCode>>,
    time: Res<Time>,
    mut query: Query<(&mut CameraMovement, &mut Transform)>,
) {
    if input.is_changed() {
        let mut velocity = Vec3::ZERO;
        let (mut camera_movement, mut transform) = query.single_mut();

        let elapsed = time.delta_seconds();

        if input.pressed(KeyCode::LControl) {
            camera_movement.speed = 100.0;
        } else {
            camera_movement.speed = 10.0;
        }

        // Forward: KeyCode::W
        // Backward: KeyCode::S
        if input.pressed(KeyCode::W) {
            velocity -= transform.rotation * Vec3::Z;
        } else if input.pressed(KeyCode::S) {
            velocity += transform.rotation * Vec3::Z;
        }

        // Left: KeyCode::A
        // Right: KeyCode::D
        if input.pressed(KeyCode::A) {
            velocity -= transform.rotation * Vec3::X;
        } else if input.pressed(KeyCode::D) {
            velocity += transform.rotation * Vec3::X;
        }

        // Up: KeyCode::Space
        // Down: KeyCode::Shift
        if input.pressed(KeyCode::Space) {
            velocity += transform.rotation * Vec3::Y;
        } else if input.pressed(KeyCode::LShift) {
            velocity -= transform.rotation * Vec3::Y;
        }

        transform.translation += velocity * elapsed * camera_movement.speed;
    }
}

fn player_camera_motion_system(
    time: Res<Time>,
    mut player_camera_movement: ResMut<PlayerCameraMovement>,
    mut mouse_motion_event_reader: EventReader<MouseMotion>,
    mut mouse_scrool_event_reader: EventReader<MouseWheel>,
    mut camera_query: Query<&mut CameraMotion, Without<PlayerMovement>>,
    mut player_query: Query<&mut Transform, With<PlayerMovement>>,
) {
    let mut delta: Vec2 = Vec2::ZERO;
    for event in mouse_motion_event_reader.iter() {
        delta += event.delta;
    }

    let elapsed = time.delta_seconds();
    let mut camera_motion = camera_query.single_mut();
    let mut player_transform = player_query.single_mut();

    let yaw_rot = camera_motion.get_horizontal_rotation(&delta, elapsed);
    let pitch_rot = camera_motion.get_vertical_rotation(&delta, elapsed);
    for event in mouse_scrool_event_reader.iter() {
        player_camera_movement.zoom -= event.y;
    }
    player_camera_movement.pitch = 15.0 + 20.0 * -pitch_rot.x;
    player_transform.rotation = yaw_rot;
}

fn player_camera_movement_system(
    player_camera_movement: Res<PlayerCameraMovement>,
    player_query: Query<&Transform, (With<PlayerMovement>, Without<CameraMovement>)>,
    mut camera_query: Query<&mut Transform, (With<CameraMovement>, Without<PlayerMovement>)>,
) {
    let player_transform = player_query.single();
    let mut camera_transform = camera_query.single_mut();

    camera_transform.translation = player_transform.translation;
    camera_transform.translation -=
        player_transform.rotation * Vec3::X * player_camera_movement.zoom;
    camera_transform.translation +=
        player_transform.rotation * Vec3::Y * player_camera_movement.pitch;
    camera_transform.look_at(player_transform.translation, Vec3::Y);
}
