FROM rust:latest as planner
WORKDIR /cache
COPY . .
RUN cargo install cargo-chef
RUN cargo chef prepare --recipe-path recipe.json

FROM rust:latest as cacher
WORKDIR /cache
COPY --from=planner /cache/recipe.json ./recipe.json
COPY --from=planner /usr/local/cargo/bin/cargo-chef /usr/local/cargo/bin/cargo-chef
RUN curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
RUN apt-get install -y nodejs pkg-config libasound2-dev libudev-dev
RUN npm install -g wrangler
RUN rustup component add clippy
RUN rustup component add rustfmt
RUN cargo install cargo-audit
RUN cargo install cargo-outdated
RUN cargo install cargo-tarpaulin
RUN cargo chef cook --recipe-path ./recipe.json
RUN cargo doc
RUN cargo tarpaulin --ciserver codecov --skip-clean --ignore-tests --frozen --target-dir target/tarpaulin --no-run
RUN ls -a | grep -Ev '^(target|\.\.?)$' | xargs rm -rf
