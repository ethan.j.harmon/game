# MarsValley - Game

[![codecov](https://codecov.io/gl/marsvalley/game/branch/main/graph/badge.svg?token=ZQ67JU8JHC)](https://codecov.io/gl/marsvalley/game)

## About this repository

This is the main repository for our game code.

## More information

This project is split up in different parts, this is just the code repository.  
For more information, issues and documentation just visit: [MarsValley](https://gitlab.com/marsvalley/marsvalley)

## How to contribute

Look at the [MarsValley/CONTRIBUTING.md](https://gitlab.com/marsvalley/marsvalley/-/blob/main/CONTRIBUTING.md) to find out how you can contribute. :)

## Tools

### Minimum rustc version

The minimum `rustc` version is `1.59.0`.  
For updating your `rustc` version, just look at your system or rustup manual.

### Pre-commit

Just install [pre-commit](https://pre-commit.com/) and you will `automatically format` the code automatically! Hurray!

## License

This project is licensed under the [LICENSE](EUPL 1.2 or higher). :)

## Controls

- `F` Switch between flycam and player controler
- `T` Toggle mouse capture
