/*
* Copyright (c) 2022 Daniél Kerkmann <daniel@kerkmann.dev>
* and the contributors of the MarsValley project.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use anyhow::{bail, Result};
use bevy::prelude::*;
use laminar::{Config, Packet, Socket, SocketEvent};
use std::time::{Duration, Instant};

use network::network::*;

use crate::components::Client;

pub struct ClientPlugin;

impl Plugin for ClientPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(
            SystemSet::on_update(NetworkMode::None).with_system(start_network_listener),
        )
        .add_system_set(
            SystemSet::on_enter(NetworkMode::Client(NetworkState::Starting))
                .with_system(init_socket.chain(catch_network_error)),
        )
        .add_system_set(
            SystemSet::on_update(NetworkMode::Client(NetworkState::Starting))
                .with_system(init_handshake.chain(catch_network_error)),
        )
        .add_system_set(
            SystemSet::on_update(NetworkMode::Client(NetworkState::Started))
                .with_system(operate_client)
                .with_system(stop_network_listener)
                .with_system(send_test_message)
                .with_system(debug_network_events),
        )
        .add_system_set(
            SystemSet::on_update(NetworkMode::Client(NetworkState::Stopping))
                .with_system(stop_network),
        );
    }
}

fn start_network_listener(
    input: Res<Input<KeyCode>>,
    mut client_state: ResMut<State<NetworkMode>>,
) {
    if input.is_changed() && input.just_pressed(KeyCode::F2) {
        debug!("F2 pressed, starting the client...");
        client_state
            .set(NetworkMode::Client(NetworkState::Starting))
            .unwrap();
    }
}

fn init_socket(mut commands: Commands, client: Option<Res<Client>>) -> Result<()> {
    if client.is_none() {
        debug!("There is no active client socket, creating a new one.");
        // port :0 will ask the OS for a free port, very neat. :)
        let addr = "127.0.0.1:0";
        let socket = Socket::bind_with_config(
            addr,
            Config {
                heartbeat_interval: Some(Duration::new(HEARTBEAT_INTERVAL_IN_SECONDS, 0)),
                ..Default::default()
            },
        )
        .map_err(|_| NetworkError::CreateSocket)?;
        let client = Client { socket };

        commands.insert_resource(client);
    }
    Ok(())
}

fn init_handshake(
    mut client: Option<ResMut<Client>>,
    mut network_mode_state: ResMut<State<NetworkMode>>,
) -> Result<()> {
    if let Some(client) = client.as_mut() {
        debug!("Send ping to the server, wish me luck ...");
        let server = SERVER_ADDRESS
            .parse()
            .map_err(|_| NetworkError::Handshake)?;
        client
            .socket
            .send(Packet::reliable_unordered(
                server,
                "ping".as_bytes().to_vec(),
            ))
            .map_err(|_| NetworkError::Handshake)?;

        network_mode_state
            .set(NetworkMode::Client(NetworkState::Started))
            .unwrap();
        Ok(())
    } else {
        bail!(NetworkError::Handshake)
    }
}

fn operate_client(mut client: ResMut<Client>, mut network_event_write: EventWriter<NetworkEvent>) {
    client.socket.manual_poll(Instant::now());

    for event in client.socket.get_event_receiver().try_iter() {
        let network_event = match event {
            SocketEvent::Connect(address) => NetworkEvent::Connect(address),
            SocketEvent::Timeout(address) => NetworkEvent::Timeout(address),
            SocketEvent::Disconnect(address) => NetworkEvent::Disconnect(address),
            SocketEvent::Packet(packet) => {
                let socket = packet.addr();
                if socket != SERVER_ADDRESS.parse().unwrap() {
                    warn!("Received weird message, wish wasn't sent from the server.");
                    continue;
                }
                let message = String::from_utf8_lossy(packet.payload()).to_string();
                NetworkEvent::Message(socket, message)
            }
        };
        network_event_write.send(network_event);
    }
}

fn stop_network_listener(input: Res<Input<KeyCode>>, mut client_state: ResMut<State<NetworkMode>>) {
    if input.just_pressed(KeyCode::F3) {
        client_state
            .set(NetworkMode::Client(NetworkState::Stopping))
            .unwrap();
    }
}

fn stop_network(mut commands: Commands, mut client_state: ResMut<State<NetworkMode>>) {
    commands.remove_resource::<Client>();
    client_state.set(NetworkMode::None).unwrap();
}

fn send_test_message(input: Res<Input<KeyCode>>, mut client: ResMut<Client>) {
    if input.just_pressed(KeyCode::F4) {
        debug!("Send message to the server, wish me luck ...");
        let server = SERVER_ADDRESS.parse().unwrap();

        client
            .socket
            .send(Packet::reliable_unordered(
                server,
                "From Server, Whoop, Whopp!".as_bytes().to_vec(),
            ))
            .unwrap();
    }
}
