/*
* Copyright (c) 2022 Daniél Kerkmann <daniel@kerkmann.dev>
* and the contributors of the MarsValley project.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use anyhow::Result;
use bevy::prelude::*;
use std::net::SocketAddr;
use thiserror::Error;

pub use crate::states::*;

pub const SERVER_ADDRESS: &str = "127.0.0.1:12351";

#[derive(Debug)]
pub enum NetworkEvent {
    Connect(SocketAddr),
    Timeout(SocketAddr),
    Disconnect(SocketAddr),
    Message(SocketAddr, String),
}

#[derive(Error, Debug)]
pub enum NetworkError {
    #[error("unable to start the socket")]
    CreateSocket,
    #[error("unable to handshake with the server")]
    Handshake,
}

pub fn debug_network_events(mut network_event_reader: EventReader<NetworkEvent>) {
    for event in network_event_reader.iter() {
        eprintln!("NetworkEvent received: {:?}", event);
    }
}

pub fn catch_network_error(
    In(previous_result): In<Result<()>>,
    mut network_mode_state: ResMut<State<NetworkMode>>,
) {
    if previous_result.is_err() {
        error!("NetworkError catched: {:?}", previous_result.unwrap_err());
        network_mode_state.set(NetworkMode::None).unwrap();
    }
}

pub const HEARTBEAT_INTERVAL_IN_SECONDS: u64 = 1;
