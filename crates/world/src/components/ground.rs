/*
* Copyright (c) 2022 Daniél Kerkmann <daniel@kerkmann.dev>
* and the contributors of the MarsValley project.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/
use bevy::prelude::*;
use bevy::render::render_resource::PrimitiveTopology;
use bevy_rapier3d::prelude::*;
use noise::{NoiseFn, OpenSimplex};

use crate::config::CHUNK_SIZE;

pub struct GroundCollider {
    vertices: Vec<Vec3>,
    indices: Vec<[u32; 3]>,
}

impl GroundCollider {
    fn new(max_vertices: usize, max_indices: usize) -> Self {
        Self {
            vertices: Vec::with_capacity(max_vertices),
            indices: Vec::with_capacity(max_indices),
        }
    }
}

impl From<GroundCollider> for Collider {
    fn from(ground_collider: GroundCollider) -> Self {
        Collider::trimesh(ground_collider.vertices, ground_collider.indices)
    }
}

pub struct GroundMesh {
    vertices: Vec<[f32; 3]>,
    indices: Vec<u32>,
    normals: Vec<[f32; 3]>,
    uvs: Vec<[f32; 2]>,
}

impl GroundMesh {
    fn new(max_vertices: usize, max_indices: usize) -> Self {
        Self {
            vertices: Vec::with_capacity(max_vertices),
            indices: Vec::with_capacity(max_indices),
            normals: Vec::with_capacity(max_vertices),
            uvs: Vec::with_capacity(max_vertices),
        }
    }
}

impl From<GroundMesh> for Mesh {
    fn from(ground_mesh: GroundMesh) -> Self {
        let mut mesh = Mesh::new(PrimitiveTopology::TriangleList);
        mesh.set_indices(Some(bevy::render::mesh::Indices::U32(ground_mesh.indices)));
        mesh.insert_attribute(Mesh::ATTRIBUTE_POSITION, ground_mesh.vertices);
        mesh.insert_attribute(Mesh::ATTRIBUTE_NORMAL, ground_mesh.normals);
        mesh.insert_attribute(Mesh::ATTRIBUTE_UV_0, ground_mesh.uvs);
        mesh
    }
}

#[derive(Component)]
pub struct Ground {
    pub collider: Collider,
    pub mesh: Mesh,
    pub offset_x: i64,
    pub offset_z: i64,
}

impl Ground {
    pub async fn new(offset_x: i64, offset_z: i64) -> Self {
        let max_vertices = ((CHUNK_SIZE + 1) * (CHUNK_SIZE + 1)) as usize;
        let max_indices = (4 * CHUNK_SIZE) as usize;

        let mut ground_mesh = GroundMesh::new(max_vertices, max_indices);
        let mut ground_collider = GroundCollider::new(max_vertices, max_indices);

        let scale_noise: f64 = 0.05;
        let scale_height: f64 = 10.0;
        let noise = OpenSimplex::default();

        let start_x = offset_x * CHUNK_SIZE;
        let start_z = offset_z * CHUNK_SIZE;
        for z in 0..=CHUNK_SIZE {
            for x in 0..=CHUNK_SIZE {
                let pos_x = x as f32;
                let pos_y = calculate_height(
                    noise,
                    (start_x + x) as f64,
                    (start_z + z) as f64,
                    scale_noise,
                    scale_height,
                );
                let pos_z = z as f32;

                ground_mesh.vertices.push([pos_x, pos_y, pos_z]);

                let up = calculate_height(
                    noise,
                    (start_x + x) as f64,
                    (start_z + z + 1) as f64,
                    scale_noise,
                    scale_height,
                );
                let down = calculate_height(
                    noise,
                    (start_x + x) as f64,
                    (start_z + z - 1) as f64,
                    scale_noise,
                    scale_height,
                );
                let left = calculate_height(
                    noise,
                    (start_x + x - 1) as f64,
                    (start_z + z) as f64,
                    scale_noise,
                    scale_height,
                );
                let right = calculate_height(
                    noise,
                    (start_x + x + 1) as f64,
                    (start_z + z) as f64,
                    scale_noise,
                    scale_height,
                );
                ground_mesh
                    .normals
                    .push(Vec3::new(left - right, 2.0, up - down).normalize().into());
                ground_mesh
                    .uvs
                    .push([x as f32 / CHUNK_SIZE as f32, z as f32 / CHUNK_SIZE as f32]);

                ground_collider
                    .vertices
                    .push(Vec3::new(pos_x, pos_y, pos_z));

                if x == CHUNK_SIZE || z == CHUNK_SIZE {
                    continue;
                }

                let point_bottom_left = (z * (CHUNK_SIZE + 1) + x) as u32;
                let point_bottom_right = point_bottom_left + 1;
                let point_top_left = ((z + 1) * (CHUNK_SIZE + 1) + x) as u32;
                let point_top_right = point_top_left + 1;

                ground_mesh.indices.push(point_bottom_left);
                ground_mesh.indices.push(point_top_left);
                ground_mesh.indices.push(point_bottom_right);
                ground_mesh.indices.push(point_top_left);
                ground_mesh.indices.push(point_top_right);
                ground_mesh.indices.push(point_bottom_right);

                ground_collider.indices.push([
                    point_bottom_left,
                    point_top_left,
                    point_bottom_right,
                ]);
                ground_collider
                    .indices
                    .push([point_top_left, point_top_right, point_bottom_right]);
            }
        }

        Self {
            collider: ground_collider.into(),
            mesh: ground_mesh.into(),
            offset_x,
            offset_z,
        }
    }
}

fn calculate_height(
    perlin: OpenSimplex,
    x: f64,
    z: f64,
    scale_width: f64,
    scale_height: f64,
) -> f32 {
    (perlin.get([x * scale_width, z * scale_width]) * scale_height) as f32
}
