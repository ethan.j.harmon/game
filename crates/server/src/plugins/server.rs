/*
* Copyright (c) 2022 Daniél Kerkmann <daniel@kerkmann.dev>
* and the contributors of the MarsValley project.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
* the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* SPDX-License-Identifier: EUPL-1.2
*/

use anyhow::Result;
use bevy::prelude::*;
use laminar::{Config, Packet, Socket, SocketEvent};
use std::{collections::HashSet, thread, time::Duration};

use network::network::*;

use crate::components::Server;

pub struct ServerPlugin;

impl Plugin for ServerPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<NetworkEvent>()
            .add_system_set(
                SystemSet::on_update(NetworkMode::None).with_system(start_network_listener),
            )
            .add_system_set(
                SystemSet::on_enter(NetworkMode::Server(NetworkState::Starting))
                    .with_system(init_socket.chain(catch_network_error)),
            )
            .add_system_set(
                SystemSet::on_update(NetworkMode::Server(NetworkState::Started))
                    .with_system(operate_server)
                    .with_system(stop_network_listener)
                    // Debug
                    .with_system(send_test_message)
                    .with_system(debug_network_events)
                    // NetworkEventListener
                    .with_system(register_connected_clients)
                    .with_system(unregister_disconnected_clients)
                    .with_system(handhake_response),
            )
            .add_system_set(
                SystemSet::on_update(NetworkMode::Server(NetworkState::Stopping))
                    .with_system(stop_network),
            );
    }
}

fn start_network_listener(
    input: Res<Input<KeyCode>>,
    mut server_state: ResMut<State<NetworkMode>>,
) {
    if input.is_changed() && input.just_pressed(KeyCode::F1) {
        debug!("F1 pressed, starting the server...");

        server_state
            .set(NetworkMode::Server(NetworkState::Starting))
            .unwrap();
    }
}

fn init_socket(
    mut commands: Commands,
    server: Option<Res<Server>>,
    mut network_mode_state: ResMut<State<NetworkMode>>,
) -> Result<()> {
    if server.is_none() {
        debug!("There is no active client socket, creating a new one.");
        let mut socket = Socket::bind_with_config(
            SERVER_ADDRESS,
            Config {
                heartbeat_interval: Some(Duration::new(HEARTBEAT_INTERVAL_IN_SECONDS, 0)),
                ..Default::default()
            },
        )
        .map_err(|_| NetworkError::CreateSocket)?;
        let (sender, receiver) = (socket.get_packet_sender(), socket.get_event_receiver());
        thread::spawn(move || socket.start_polling());

        info!("Server created successfully!");
        commands.insert_resource(Server {
            sender,
            receiver,
            clients: HashSet::new(),
        });

        network_mode_state
            .overwrite_set(NetworkMode::Server(NetworkState::Started))
            .expect("nani");
        Ok(())
    } else {
        Ok(())
    }
}

fn operate_server(server: Res<Server>, mut network_event_write: EventWriter<NetworkEvent>) {
    for event in server.receiver.try_iter() {
        let network_event = match event {
            SocketEvent::Connect(address) => NetworkEvent::Connect(address),
            SocketEvent::Timeout(address) => NetworkEvent::Timeout(address),
            SocketEvent::Disconnect(address) => NetworkEvent::Disconnect(address),
            SocketEvent::Packet(packet) => NetworkEvent::Message(
                packet.addr(),
                String::from_utf8_lossy(packet.payload()).to_string(),
            ),
        };
        network_event_write.send(network_event);
    }
}

fn stop_network_listener(input: Res<Input<KeyCode>>, mut server_state: ResMut<State<NetworkMode>>) {
    if input.just_pressed(KeyCode::F3) {
        server_state
            .set(NetworkMode::Server(NetworkState::Stopping))
            .unwrap();
    }
}

fn stop_network(mut commands: Commands, mut server_state: ResMut<State<NetworkMode>>) {
    commands.remove_resource::<Server>();
    server_state.set(NetworkMode::None).unwrap();
}

fn send_test_message(input: Res<Input<KeyCode>>, server: Res<Server>) {
    if input.just_pressed(KeyCode::F4) {
        debug!("Send message to all clients, wish me luck ...");
        for socket in server.clients.iter() {
            debug!("Found client, with socket: {:?}", socket);
            server
                .sender
                .send(Packet::reliable_unordered(
                    socket.to_owned(),
                    "From Server, Whoop, Whopp!".as_bytes().to_vec(),
                ))
                .unwrap();
        }
    }
}

fn handhake_response(server: Res<Server>, mut network_event_reader: EventReader<NetworkEvent>) {
    for event in network_event_reader.iter() {
        if let NetworkEvent::Message(socket, message) = event {
            if message == "ping" {
                server
                    .sender
                    .send(Packet::reliable_unordered(
                        socket.to_owned(),
                        "pong".as_bytes().to_vec(),
                    ))
                    .unwrap();
            }
        }
    }
}

fn register_connected_clients(
    mut server: ResMut<Server>,
    mut network_event_reader: EventReader<NetworkEvent>,
) {
    for event in network_event_reader.iter() {
        if let NetworkEvent::Connect(address) = event {
            server.clients.insert(address.to_owned());
        }
    }
}

fn unregister_disconnected_clients(
    mut server: ResMut<Server>,
    mut network_event_reader: EventReader<NetworkEvent>,
) {
    for event in network_event_reader.iter() {
        if let NetworkEvent::Disconnect(address) = event {
            server.clients.remove(address);
        }
    }
}
